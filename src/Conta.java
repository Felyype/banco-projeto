import java.util.Scanner;

public class Conta {
	
	/*
	 * Criar contas
	 */
	
	public String nomeCompleto() {
		Scanner pegarNomeCompleto = new Scanner(System.in);
		String resultadoNomeCompleto = pegarNomeCompleto.next();
		
		return resultadoNomeCompleto;
	}
	
	public String senhaClientes() {
		Scanner pegarSenha = new Scanner(System.in);
		String resultadoPegarSenha = pegarSenha.next();
		
		return resultadoPegarSenha;
	}
	
	public String cpfClientes() {
		Scanner pegarCpfClientes = new Scanner(System.in);
		String resultadoCpfClientes = pegarCpfClientes.next();
		
		return resultadoCpfClientes;
	}
}
