import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProgramaContas {
	public static void main(String[] args) {
		Conta conta = new Conta();
		Settings sets = new Settings();
		
		int saldo = Settings.saldoDoBanco;
		int transacaoAnterior;
		
		
		sets.menuWelcome();
		
		
		
		System.out.println("- Nome Completo ( Sem espa�os )."); 
		
		List<String> nomeClientes = new ArrayList();
		String resultadoNomeCompleto = conta.nomeCompleto();
		nomeClientes.add(resultadoNomeCompleto);
		
		System.out.println("- Senha ( N�meros e letras )."); 
		
		List<String> senhaClientes = new ArrayList();
		String resultadoSenhaClientes = conta.senhaClientes();
		senhaClientes.add(resultadoSenhaClientes);
		
		System.out.println("- CPF ( Apenas n�meros )."); 
		
		List<String> cpfClientes = new ArrayList();
		String resultadoCpfClientes = conta.cpfClientes();
		cpfClientes.add(resultadoCpfClientes);
		
		
		
		boolean looping = true;
		while(looping) {
			
			
			
			System.out.println("");
			System.out.println("* Bem vindo de volta " +nomeClientes.get(0)+ ".");
			sets.menuFuncoes();
			
			
			Scanner decisao = new Scanner (System.in);
			String decisaoValor = decisao.next();
			
		switch(decisaoValor) {
			case "1":{
				System.out.println("");
				System.out.println("- Deposite o valor desejado");
				
				Scanner valorDeposito = new Scanner (System.in);
				int quantidadeDeposito = valorDeposito.nextInt();
				
				if (quantidadeDeposito > 0) {
					saldo = saldo + quantidadeDeposito;
					transacaoAnterior = quantidadeDeposito;
				}
				System.out.println("Voc� acabou de depositar " +quantidadeDeposito+ "R$ tendo agora um saldo atual de = " +saldo+ "R$");
				break;
		}
			case "2":{
				System.out.println("");
				System.out.println("- Retire o valor desejado");
				
				Scanner valorRetirada = new Scanner (System.in);
				int quantidadeRetirada = valorRetirada.nextInt();
				
				if (quantidadeRetirada > 0) {
					saldo = saldo - quantidadeRetirada;
				}
				System.out.println("Voc acabou de retirar " +quantidadeRetirada+ "R$ tendo agora um saldo atual de = " +saldo+ "R$");
				break;
			}
			case "3":{
				System.out.println("");
				System.out.println("Seu saldo atual  de = " +saldo+ "R$");
				break;
			}
			case "4":{
				System.out.println("");
				System.out.println("*********** DADOS ***********");
				System.out.println("");
				System.out.println("- Seu Nome  " +nomeClientes.get(0)+ ".");
				System.out.println("- Sua Senha  " +senhaClientes.get(0)+ ".");
				System.out.println("- Seu CPF  " +cpfClientes.get(0)+ ".");
				break;
			}
			case "5":{
				System.out.println("At� a proxima " +nomeClientes.get(0)+ "!");
				System.exit(0);
				break;
			}
      }
	}
  }
}
